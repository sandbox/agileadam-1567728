<?php

/**
 * Page callback for main form
 */
function usermoderate_main($targetuser) {
  $output = '<h2>' . t('Moderate user: %name', array('%name' => $targetuser->name)) . '</h2>';

  $validating_id = logintoboggan_validating_id();
  $pre_auth = ($validating_id != DRUPAL_AUTHENTICATED_RID && array_key_exists($validating_id, $targetuser->roles));

  if ($pre_auth && $targetuser->status == 1) {
    $output .= drupal_get_form('usermoderate_mainform', $targetuser);
  }

  if (!$pre_auth && $targetuser->status == 1) {
    $output .= t('This account has already been approved.');
  }

  if ($targetuser->status == 0) {
    $output .= t('This account has already been rejected.');
  }

  return $output;
}

/**
 * Define the main form
 */
function usermoderate_mainform($form_state, $targetuser) {
  $form['targetuser'] = array(
    '#type' => 'value',
    '#value' => $targetuser,
  );

  $form['instructions'] = array(
    '#value' => '<div><strong>You will NOT be asked to confirm your choice!<br />Proceed with caution!</strong></div><br />',
  );

  $form['approve'] = array(
    '#prefix' => '<div>',
    '#type' => 'submit',
    '#value' => t('APPROVE'),
    '#suffix' => ' this account. The user will receive an email explaining that their account has been approved.</div><hr>',
  );

  $form['reject'] = array(
    '#prefix' => '<div>',
    '#type' => 'submit',
    '#value' => t('REJECT'),
    '#suffix' => ' this account. The user will receive an email explaining that they have been denied access.</div>',
  );

  $form['#redirect'] = 'user/' . $targetuser->uid;

  return $form;
}

/**
 * Submission handler for the main form
 */
function usermoderate_mainform_submit($form, &$form_state) {
  // Handle APPROVE action
  if ($form_state['clicked_button']['#id'] == 'edit-approve') {
    usermoderate_approve_account($form_state['values']['targetuser']);
  }

  // Handle REJECT action
  if ($form_state['clicked_button']['#id'] == 'edit-reject') {
    usermoderate_reject_account($form_state['values']['targetuser']);
  }
}

/**
 * Approve an account
 * 
 * @param $account
 *   A user object
 */
function usermoderate_approve_account($account) {
  $validating_id = logintoboggan_validating_id();

  $account->roles[DRUPAL_AUTHENTICATED_RID] = 'authenticated user';
  unset($account->roles[$validating_id]);
  $edit = array('status' => 1, 'roles' => $account->roles);
  user_save($account, $edit);

  $modified_user = user_load($account->uid);
  if (!array_key_exists($validating_id, $modified_user->roles)) {
    drupal_set_message('User account has been authorized.');
  }
  else {
    drupal_set_message('User account could not be authorized.', 'error');
  }

  _user_mail_notify('status_activated', $account);
}

/**
 * Reject an account
 * 
 * @param $account
 *   A user object
 */
function usermoderate_reject_account($account) {
  // We'll leave the pre_authenticated role for now
  // as an indicator that the account was rejected right away
  // and not just blocked later
  user_save($account, array('status' => 0));

  $modified_user = user_load($account->uid);
  if ($modified_user->status == 0) {
    drupal_set_message('User account has been blocked.');
  }
  else {
    drupal_set_message('User account could not be blocked.', 'error');
  }
}
